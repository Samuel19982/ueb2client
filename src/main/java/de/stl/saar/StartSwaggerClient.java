package de.stl.saar;

import de.stl.saar.view.MainApplication;
import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartSwaggerClient{

	public static void main(String[] args) {
		Application.launch(MainApplication.class,args);
	}
}
