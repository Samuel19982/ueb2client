package de.stl.saar.constants;


public class FXConstants {
	private static final String CSS_PATH = "/de/stl/saar/view/controller/css";


	public static final String TRAY_ICON = "/de/stl/saar/view/controller/assets/swagger.png";

	public static final String PATH_DIALOG_STYLESHEET = CSS_PATH+"/dialogs.css";
	public static final String PATH_DARKTHEME_STYLESHEET = CSS_PATH+"/darktheme.css";
	private static final String BASE_PATH = "/view/controller/";
	public static final String SUFFIX_FXML = ".fxml";

	public static final String PATH_COMPANY_VIEW = BASE_PATH+"companyView"+SUFFIX_FXML;
	public static final String PATH_CREATE_COMPANY = BASE_PATH+"createCompany"+SUFFIX_FXML;
}
