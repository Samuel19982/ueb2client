/*
 * app_store
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.stl.saar.client.model;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

import javax.validation.constraints.NotBlank;

/**
 * AppPostRequest
 */
@Builder
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2021-10-29T10:47:21.205Z[GMT]")
public class AppPostRequest {
  @SerializedName("title")
  @NotBlank
  private String title = null;

  @SerializedName("description")
  @NotBlank
  private String description = null;

  @SerializedName("package")
  @NotBlank
  private String _package = null;

  @SerializedName("companyId")
  @NotBlank
  private String companyId = null;

  public AppPostRequest title(String title) {
    this.title = title;
    return this;
  }

  public AppPostRequest(final String title, final String description, final String _package, final String companyId) {
    this.title = title;
    this.description = description;
    this._package = _package;
    this.companyId = companyId;
  }

  /**
   * Get title
   * @return title
  **/
  @Schema(required = true, description = "")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public AppPostRequest description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @Schema(description = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public AppPostRequest _package(String _package) {
    this._package = _package;
    return this;
  }

   /**
   * Get _package
   * @return _package
  **/
  @Schema(required = true, description = "")
  public String getPackage() {
    return _package;
  }

  public void setPackage(String _package) {
    this._package = _package;
  }

  public AppPostRequest companyId(String companyId) {
    this.companyId = companyId;
    return this;
  }

   /**
   * Get companyId
   * @return companyId
  **/
  @Schema(required = true, description = "")
  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AppPostRequest appPostRequest = (AppPostRequest) o;
    return Objects.equals(this.title, appPostRequest.title) &&
        Objects.equals(this.description, appPostRequest.description) &&
        Objects.equals(this._package, appPostRequest._package) &&
        Objects.equals(this.companyId, appPostRequest.companyId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, description, _package, companyId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AppPostRequest {\n");
    
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    _package: ").append(toIndentedString(_package)).append("\n");
    sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
