/*
 * app_store
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package de.stl.saar.client.api;

import com.google.gson.reflect.TypeToken;
import de.stl.saar.client.*;
import de.stl.saar.client.model.Rating;
import de.stl.saar.client.model.RatingPostRequest;

import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class RatingsApi {
    private ApiClient apiClient;

    public RatingsApi() {
        this(Configuration.getDefaultApiClient());
    }

    public RatingsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for apiV1AppsAppIdRatingsGet
     * @param appId the id of the app (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public Call apiV1AppsAppIdRatingsGetCall(String appId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/api/v1/Apps/{appId}/Ratings"
            .replaceAll("\\{" + "appId" + "\\}", apiClient.escapeString(appId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private Call apiV1AppsAppIdRatingsGetValidateBeforeCall(String appId, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        // verify the required parameter 'appId' is set
        if (appId == null) {
            throw new ApiException("Missing the required parameter 'appId' when calling apiV1AppsAppIdRatingsGet(Async)");
        }
        
        Call call = apiV1AppsAppIdRatingsGetCall(appId, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Returns all ratings of an app
     * 
     * @param appId the id of the app (required)
     * @return List&lt;Rating&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Rating> apiV1AppsAppIdRatingsGet(String appId) throws ApiException {
        ApiResponse<List<Rating>> resp = apiV1AppsAppIdRatingsGetWithHttpInfo(appId);
        return resp.getData();
    }

    /**
     * Returns all ratings of an app
     * 
     * @param appId the id of the app (required)
     * @return ApiResponse&lt;List&lt;Rating&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Rating>> apiV1AppsAppIdRatingsGetWithHttpInfo(String appId) throws ApiException {
        Call call = apiV1AppsAppIdRatingsGetValidateBeforeCall(appId, null, null);
        Type localVarReturnType = new TypeToken<List<Rating>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Returns all ratings of an app (asynchronously)
     * 
     * @param appId the id of the app (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public Call apiV1AppsAppIdRatingsGetAsync(String appId, final ApiCallback<List<Rating>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        Call call = apiV1AppsAppIdRatingsGetValidateBeforeCall(appId, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Rating>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for apiV1AppsAppIdRatingsIdDelete
     * @param appId the id of the app (required)
     * @param id the id of the version (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public Call apiV1AppsAppIdRatingsIdDeleteCall(String appId, String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/api/v1/Apps/{appId}/Ratings/{id}"
            .replaceAll("\\{" + "appId" + "\\}", apiClient.escapeString(appId.toString()))
            .replaceAll("\\{" + "id" + "\\}", apiClient.escapeString(id.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private Call apiV1AppsAppIdRatingsIdDeleteValidateBeforeCall(String appId, String id, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        // verify the required parameter 'appId' is set
        if (appId == null) {
            throw new ApiException("Missing the required parameter 'appId' when calling apiV1AppsAppIdRatingsIdDelete(Async)");
        }
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new ApiException("Missing the required parameter 'id' when calling apiV1AppsAppIdRatingsIdDelete(Async)");
        }
        
        Call call = apiV1AppsAppIdRatingsIdDeleteCall(appId, id, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Deletes a rating of an app
     * 
     * @param appId the id of the app (required)
     * @param id the id of the version (required)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public void apiV1AppsAppIdRatingsIdDelete(String appId, String id) throws ApiException {
        apiV1AppsAppIdRatingsIdDeleteWithHttpInfo(appId, id);
    }

    /**
     * Deletes a rating of an app
     * 
     * @param appId the id of the app (required)
     * @param id the id of the version (required)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Void> apiV1AppsAppIdRatingsIdDeleteWithHttpInfo(String appId, String id) throws ApiException {
        Call call = apiV1AppsAppIdRatingsIdDeleteValidateBeforeCall(appId, id, null, null);
        return apiClient.execute(call);
    }

    /**
     * Deletes a rating of an app (asynchronously)
     * 
     * @param appId the id of the app (required)
     * @param id the id of the version (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public Call apiV1AppsAppIdRatingsIdDeleteAsync(String appId, String id, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        Call call = apiV1AppsAppIdRatingsIdDeleteValidateBeforeCall(appId, id, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    /**
     * Build call for apiV1AppsAppIdRatingsPost
     * @param appId the id of the app (required)
     * @param body the rating to create (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public Call apiV1AppsAppIdRatingsPostCall(String appId, RatingPostRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = body;
        
        // create path and map variables
        String localVarPath = "/api/v1/Apps/{appId}/Ratings"
            .replaceAll("\\{" + "appId" + "\\}", apiClient.escapeString(appId.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json", "text/json", "application/_*+json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private Call apiV1AppsAppIdRatingsPostValidateBeforeCall(String appId, RatingPostRequest body, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        // verify the required parameter 'appId' is set
        if (appId == null) {
            throw new ApiException("Missing the required parameter 'appId' when calling apiV1AppsAppIdRatingsPost(Async)");
        }
        
        Call call = apiV1AppsAppIdRatingsPostCall(appId, body, progressListener, progressRequestListener);
        return call;

        
        
        
        
    }

    /**
     * Creates a new rating for an app
     * 
     * @param appId the id of the app (required)
     * @param body the rating to create (optional)
     * @return Rating
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Rating apiV1AppsAppIdRatingsPost(String appId, RatingPostRequest body) throws ApiException {
        ApiResponse<Rating> resp = apiV1AppsAppIdRatingsPostWithHttpInfo(appId, body);
        return resp.getData();
    }

    /**
     * Creates a new rating for an app
     * 
     * @param appId the id of the app (required)
     * @param body the rating to create (optional)
     * @return ApiResponse&lt;Rating&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Rating> apiV1AppsAppIdRatingsPostWithHttpInfo(String appId, RatingPostRequest body) throws ApiException {
        Call call = apiV1AppsAppIdRatingsPostValidateBeforeCall(appId, body, null, null);
        Type localVarReturnType = new TypeToken<Rating>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates a new rating for an app (asynchronously)
     * 
     * @param appId the id of the app (required)
     * @param body the rating to create (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public Call apiV1AppsAppIdRatingsPostAsync(String appId, RatingPostRequest body, final ApiCallback<Rating> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        Call call = apiV1AppsAppIdRatingsPostValidateBeforeCall(appId, body, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Rating>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
