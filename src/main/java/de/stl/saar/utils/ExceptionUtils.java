package de.stl.saar.utils;

import de.stl.saar.client.model.*;
import de.stl.saar.exceptions.GlobalException;
import lombok.experimental.UtilityClass;

import javax.validation.ConstraintDeclarationException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@UtilityClass
public class ExceptionUtils {
	private static Validator validator = null;


	static{
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	public static String getNoItemSelected(){
		return "Kein Element ausgewählt";
	}


	public static String getNoItemSelectedDetailed(){
		return "Für diese Funktion muss ein Element aus der Liste ausgewählt werden";
	}


	public static void checkException(final Set<? extends ConstraintViolation<?>> setOfViolations){
		if (setOfViolations.size()>0){
			throw new GlobalException(setOfViolations);
		}
	}

	public static void validateCompanyPostRequest(final CompanyPostRequest companyPostRequest){
		checkException(validator.validate(companyPostRequest));
	}

	public static void validateAddress(final Address address){
		checkException(validator.validate(address));
	}

	public static void validateApp(final App app){
		checkException(validator.validate(app));
	}

	public static void validateAppPostRequest(final AppPostRequest appPostRequest){
		checkException(validator.validate(appPostRequest));
	}

	public static void validateAppVersionPostRequest(final AppVersionPostRequest appVersionPostRequest){
		checkException(validator.validate(appVersionPostRequest));
	}

	public static void validateCompany(final Company companyToEdit) {
		checkException(validator.validate(companyToEdit));
	}

	public static void validateRatingPostRequest(final RatingPostRequest ratingPostRequest) {
		checkException(validator.validate(ratingPostRequest));
	}

	public static void validateScreenshotPostRequest(final ScreenshotPostRequest screenshotPostRequest) {
		checkException(validator.validate(screenshotPostRequest));
	}
}
