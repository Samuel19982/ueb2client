package de.stl.saar.utils;

import de.stl.saar.constants.FXConstants;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.Objects;

@UtilityClass
public class JavaFXUtils {
	private final static String ID_OK = "Ok";
	private final static String ID_CANCEL = "Cancel";

	public static void showErrorMessage(final String header, final String contentText){
		Alert errorAlert = new Alert(Alert.AlertType.ERROR);
		Stage stage = (Stage) errorAlert.getDialogPane().getScene().getWindow();
		stage.centerOnScreen();
		errorAlert.initModality(Modality.APPLICATION_MODAL);
		stage.setAlwaysOnTop(true);
		errorAlert.setHeaderText(header);
		layout(errorAlert);
		errorAlert.setContentText(contentText);
		errorAlert.showAndWait();
	}

	private static void layout(Dialog<?> dialog) {
		setDefaultCssId(dialog);
		setDefaultStylesheets(dialog);
	}
	/**
	 * Setzt die Id's fuer den Abbrechen- und OK- Button, damit diese gestylt werden koennen.
	 * @param dialog der Dialog
	 */
	private static void setDefaultCssId(Dialog<?> dialog) {
		Button btnOk = null;
		Button btnCancel = null;
		DialogPane dialogPane = dialog.getDialogPane();
		if (dialogPane.lookupButton(ButtonType.OK) instanceof Button) {
			btnOk = (Button) dialogPane.lookupButton(ButtonType.OK);
		}
		if (dialogPane.lookupButton(ButtonType.CANCEL) instanceof Button) {
			btnCancel = (Button) dialogPane.lookupButton(ButtonType.CANCEL);
		}
		if (btnOk != null) {
			btnOk.setId(ID_OK);
		}
		if (btnCancel != null) {
			btnCancel.setId(ID_CANCEL);
		}
	}

	/**
	 * Fuegt die Standard-Stylesheets zum Dialog hinzu
	 * @param dialog Der Dialog, der gestylt werden soll
	 */
	private static void setDefaultStylesheets(Dialog<?> dialog) {
		List<String> styleSheets = dialog.getDialogPane().getStylesheets();
		styleSheets.add(Objects.requireNonNull(JavaFXUtils.class.getResource(FXConstants.PATH_DIALOG_STYLESHEET)).toExternalForm());
		styleSheets.add(Objects.requireNonNull(JavaFXUtils.class.getResource(FXConstants.PATH_DARKTHEME_STYLESHEET)).toExternalForm());
	}
}
