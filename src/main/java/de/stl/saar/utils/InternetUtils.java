package de.stl.saar.utils;

import de.stl.saar.client.Configuration;
import de.stl.saar.client.api.AppsApi;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.http.HttpRequest;

@UtilityClass
public class InternetUtils {

    // Sends ping request to a provided IP address
    public static boolean sendPingRequest(String url) {
        try {
            Configuration.adjustBasePath(url);

            URLConnection urlConnection = new URL(url).openConnection();

            urlConnection.connect();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}

