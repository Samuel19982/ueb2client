package de.stl.saar.factories;

import de.stl.saar.utils.InternetUtils;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ObservableValueBase;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;

import java.util.function.Function;

/**
 * Dient der Erzeugung von Callbacks.
 * @author jonas
 *
 */
public class CallbackFactory {

	private CallbackFactory() {
		
	}
	
	/**
	 * Erzeugt eine Factory, die Tabellen-Zellen erzeugt
	 * @param <S> Klasse des Zeileninhalts der Tabelle
	 * @param <T> Klasse des Spalteninhalts der Tabelle
	 * @param valueToStringMapper Gibt vor, wie der Inhalt der Zelle dargestellt werden soll
	 * @return Factory zum darstellen von Objekten in Tabellen
	 */
	public static <S,T> Callback<TableColumn<S,T>, TableCell<S,T>> 
		createTableCellFactory(Function<T,String> valueToStringMapper){
		
		return column -> new TableCell<>() {

			@Override
			public void updateItem(T value, boolean empty) {
				if (empty || value == null) {
					setText(null);
				} else {
					setText(valueToStringMapper.apply(value));
				}
			}
		};
	}
	
	/**
	 * Erzeugt eine Factory, die Werte fuer Tabellen-Zellen erzeugt
	 * @param <S> Klasse des Zeileninhalts der Tabelle
	 * @param <T> Klasse des Spalteninhalts der Tabelle
	 * @param mapper Gibt vor, wie der Inhalt der Zelle erzeugt werden soll
	 * @return Factory zum fuellen von Tabellen-Zellen
	 */
	public static <S,T> Callback<CellDataFeatures<S,T>, ObservableValue<T>> createTableCellValueFactory(Function<S,T>  mapper){
		return cellDataFeatures -> new ObservableValueBase<>() {
			@Override
			public T getValue() {
				return mapper.apply(cellDataFeatures.getValue());
			}
		};
	}
	
	
	
}
