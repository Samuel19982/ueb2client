package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.AppsApi;
import de.stl.saar.client.api.RatingsApi;
import de.stl.saar.client.model.Address;
import de.stl.saar.client.model.App;
import de.stl.saar.client.model.Company;
import de.stl.saar.client.model.Rating;
import de.stl.saar.factories.CallbackFactory;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import de.stl.saar.view.dialog.CreateEditAppDialog;
import de.stl.saar.view.dialog.CreateEditCompanyDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

@Component
@FxmlView("appsView.fxml")
public class AppsViewController {
	private final AppsApi appsApi;
	private final RatingsApi ratingsApi;
	private final CompanyViewController companyViewController;
	private final FxWeaver fxWeaver;
	public TableView<App> appsTable;


	public TableColumn<App, String> appTitle;
	public TableColumn<App, String> appDescription;
	public TableColumn<App, String> appPackage;

	private final SortedList<App> sortedFilteredList;
	private final FilteredList<App> companyFilteredList;
	private ObservableList<App> appsObservableList;
	public TableColumn<App, String> appTotalRating;

	public AppsViewController(final AppsApi appsApi,
	                          final RatingsApi ratingsApi,
	                          final CompanyViewController companyViewController,
	                          final FxWeaver fxWeaver) throws ApiException {
		this.appsApi = appsApi;
		this.ratingsApi = ratingsApi;
		this.companyViewController = companyViewController;
		this.fxWeaver = fxWeaver;
		appsObservableList = FXCollections.observableArrayList();
		companyFilteredList = new FilteredList<>(appsObservableList);
		sortedFilteredList = new SortedList<>(companyFilteredList);
	}

	private ObservableList<App> updateApps(final AppsApi appsApi) throws ApiException {
		appsObservableList.clear();
		appsObservableList = FXCollections.observableArrayList(appsApi.apiV1AppsGet(Integer.MAX_VALUE,0)
		                                                              .stream().filter(company->companyViewController
						.getCurrentlySelectedCompany().getId().equals(company.getCompanyId())).collect(Collectors.toList()));
		appsObservableList.addAll();
		return appsObservableList;
	}

	@FXML
	public void initialize() throws ApiException {

		appsTable.setItems(updateApps(appsApi));
		appTitle.setCellValueFactory(CallbackFactory.createTableCellValueFactory(App::getTitle));
		appDescription.setCellValueFactory(CallbackFactory.createTableCellValueFactory(App::getDescription));
		appPackage.setCellValueFactory(CallbackFactory.createTableCellValueFactory(App::getPackage));
		appTotalRating.setCellValueFactory(CallbackFactory.createTableCellValueFactory(app-> {
			try {
				return String.valueOf(determineTotalRatingPerApp(ratingsApi.apiV1AppsAppIdRatingsGet(app.getId())));
			}
			catch (ApiException e) {
				e.printStackTrace();
			}
			return "Bisher keine Bewertung";
		}));
	}

	public void onBtnDeleteAppClicked(final MouseEvent mouseEvent) throws ApiException {
		App selectedApp  = appsTable.getSelectionModel().getSelectedItem();
		if(selectedApp==null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}

		appsApi.deleteApp(selectedApp.getId());
		appsObservableList.remove(selectedApp);
	}

	public void onBtnAddAppClicked(final MouseEvent mouseEvent) throws IOException {
		final CreateEditAppDialog dialog = new CreateEditAppDialog(fxWeaver, companyViewController);
		dialog.openCreate();
	}

	public void onBtnReturnToCompany(final MouseEvent mouseEvent) {
		Stage stage = companyViewController.getStage();
		Parent root = fxWeaver.loadView(CompanyViewController.class);
		stage.setScene(new Scene(root, stage.getScene().getWidth(), stage.getScene().getHeight()));
	}

	private double determineTotalRatingPerApp(List<Rating> ratings){
		 OptionalDouble optionalRating =  ratings.stream().mapToDouble(Rating::getStars).average();
		 if(optionalRating.isEmpty()){
			 return 0;
		 }
		 else{
			 return optionalRating.getAsDouble();
		 }
	}

	public void updateElement(int index, App updatedApp){
		appsObservableList.set(index, updatedApp);
	}

	public int determineIndexInList(final String id){
		int counter=0;
		for (final App app: appsObservableList){
			if(app.getId().equals(id)){
				return counter;
			}
			else{
				counter++;
			}
		}
		return -1;
	}

	public ObservableList<App> getAppsObservableList() {
		return appsObservableList;
	}

	public App getCurrentlySelectedApp(){
		return appsTable.getSelectionModel().getSelectedItem();
	}

	public void onBtnShowRatings(final MouseEvent mouseEvent) {
		if(getCurrentlySelectedApp() ==null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}
		Parent root = fxWeaver.loadView(RatingsViewController.class);
		Scene scene = new Scene(root, companyViewController.getCorrectWidth(),
				companyViewController.getCorrectHeight());
		companyViewController.getStage().setScene(scene);
	}

	public void onTableViewCompanyClicked(final MouseEvent mouseEvent) throws IOException {
		if (mouseEvent.getClickCount() > 1 ) {
			final CreateEditAppDialog dialog = new CreateEditAppDialog(fxWeaver,
					companyViewController);
			dialog.openEdit(getCurrentlySelectedApp());
		}
	}

	public void onBtnShowVersions(final MouseEvent mouseEvent) {
		if(getCurrentlySelectedApp() ==null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}
		Parent root = fxWeaver.loadView(VersionsViewController.class);
		Scene scene = new Scene(root, companyViewController.getCorrectWidth(),
				companyViewController.getCorrectHeight());
		companyViewController.getStage().setScene(scene);
	}
}
