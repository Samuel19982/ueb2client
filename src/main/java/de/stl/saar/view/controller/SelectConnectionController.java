package de.stl.saar.view.controller;

import com.dustinredmond.fxtrayicon.FXTrayIcon;
import de.stl.saar.client.Configuration;
import de.stl.saar.constants.FXConstants;
import de.stl.saar.utils.InternetUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Objects;

@Component
@FxmlView("selectConnection.fxml")
public class SelectConnectionController {

	private final FxWeaver fxWeaver;
	public ComboBox<String> applicationURL;
	private Stage stage;
	private ConfigurableApplicationContext context;
	private final ObservableList<String> urls;
	private FXTrayIcon fxTrayIcon;

	public SelectConnectionController(final FxWeaver fxWeaver) {
		this.fxWeaver = fxWeaver;
		urls = FXCollections.observableList(new ArrayList<>());
	}

	@FXML
	public void initialize() throws IOException {
			// Pass in the app's main stage, and path to the icon image
		System.setProperty("java.awt.headless", "false");



		final File file = new File("connections.txt");
		if(file.exists()){
			urls.addAll(Files.readAllLines(file.toPath(), Charset.defaultCharset()));
		}
		applicationURL.setItems(urls);
	}

	public void startApp(final MouseEvent mouseEvent) {
		try {
			if(InternetUtils.sendPingRequest(applicationURL.getSelectionModel().getSelectedItem())){
				fxTrayIcon.showInfoMessage("Verbunden Der Host konnte erreicht werden");
			}
			else{
				throw new IOException("Adresse konnte nicht erreicht werden");
			}
			Configuration.adjustBasePath(applicationURL.getSelectionModel().getSelectedItem());
			Parent root = fxWeaver.loadView(CompanyViewController.class);
			Scene scene = new Scene(root, stage.getScene().getWidth(), stage.getScene().getHeight());
			stage.setScene(scene);
			final CompanyViewController companyViewController = context.getBean(CompanyViewController.class);
			companyViewController.setStage(stage);
			companyViewController.setApplicationContext(context);
		}
		catch (IOException ioException){
			fxTrayIcon.showErrorMessage("Adresse konnte nicht erreicht werden");
		}
	}

	public void setStage(final Stage stage) {
		this.stage = stage;
		fxTrayIcon = new FXTrayIcon(stage,
				Objects.requireNonNull(getClass().getResource(FXConstants.TRAY_ICON)));
		fxTrayIcon.show();
	}

	public void setApplicationContext(final ConfigurableApplicationContext applicationContext) {
		this.context = applicationContext;
	}
}
