package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.CompaniesApi;
import de.stl.saar.client.model.Address;
import de.stl.saar.client.model.Company;
import de.stl.saar.client.model.CompanyPostRequest;
import de.stl.saar.exceptions.GlobalException;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import java.util.Set;

@Component
@FxmlView("createCompany.fxml")
public class CreateCompanyController {

	private final CompanyViewController companyViewController;
	private final CompaniesApi companiesApi;
	public Label title;
	private Company companyToEdit;
	public TextField companyName;
	public TextField companyStreet;
	public TextField companyCity;
	public TextField companyLand;
	public TextField companyZipCode;
	public TextField companyCountry;
	private Stage stage;
	private boolean isEditCompany = false;

	public CreateCompanyController(final CompanyViewController companyViewController,
	                               final CompaniesApi companiesApi) {
		this.companyViewController = companyViewController;
		this.companiesApi = companiesApi;
	}

	@FXML
	public void initialize(){
		if(isEditCompany){
			title.setText("Unternehmen bearbeiten");
		}
	}

	public void btnSaveCompany(final MouseEvent mouseEvent) throws ApiException {
		try {
			final Address futureAddress = new Address(companyStreet.getText(), companyCity.getText(),
					companyLand.getText(), companyZipCode.getText(), companyCountry.getText());
			ExceptionUtils.validateAddress(futureAddress);

			if (!isEditCompany) {
				CompanyPostRequest companyPostRequest = new CompanyPostRequest(companyName.getText(), futureAddress);
				ExceptionUtils.validateCompanyPostRequest(companyPostRequest);
				Company savedCompany = companiesApi.apiV1CompaniesPost(companyPostRequest);
				companyViewController.getCompanyObservableList().add(savedCompany);
			}
			else {
				companyToEdit.setAddress(futureAddress);
				companyToEdit.setName(companyName.getText());
				ExceptionUtils.validateCompany(companyToEdit);
				companiesApi.apiV1CompaniesIdPut(companyToEdit.getId(), companyToEdit);
				final int row = companyViewController.determineIndexInList(companyToEdit.getId());
				companyViewController.updateElement(row, companyToEdit);
			}
			stage.close();
		}
		catch (final GlobalException globalException){
			JavaFXUtils.showErrorMessage(GlobalException.header, globalException.getMessage());
		}
	}

	public void setStage(final Stage stage) {
		this.stage = stage;
	}

	public void editCompany(final Company companyToEdit) {
		isEditCompany = true;
		this.companyToEdit = companyToEdit;
			companyName.setText(companyToEdit.getName());
			companyCountry.setText(companyToEdit.getAddress().getCountry());
			companyStreet.setText(companyToEdit.getAddress().getStreet());
			companyCity.setText(companyToEdit.getAddress().getCity());
			companyLand.setText(companyToEdit.getAddress().getState());
			companyZipCode.setText(companyToEdit.getAddress().getPostalCode());
	}
}
