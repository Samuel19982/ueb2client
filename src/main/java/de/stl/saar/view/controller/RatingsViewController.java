package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.AppsApi;
import de.stl.saar.client.api.RatingsApi;
import de.stl.saar.client.model.App;
import de.stl.saar.client.model.Rating;
import de.stl.saar.factories.CallbackFactory;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import de.stl.saar.view.dialog.CreateEditCompanyDialog;
import de.stl.saar.view.dialog.CreateEditRatingDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import net.rgielen.fxweaver.core.FxWeaver;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Component
@FxmlView("ratingsView.fxml")
public class RatingsViewController {

	private final RatingsApi ratingsApi;
	private final AppsApi appsApi;
	private final AppsViewController appsViewController;
	private final CompanyViewController companyViewController;
	private final FxWeaver fxWeaver;
	public TableView<Rating> ratingTable;
	public TableColumn<Rating, App> ratedApp;
	public TableColumn<Rating, String> author;
	public TableColumn<Rating, Integer> ratingStars;
	public TableColumn<Rating, String> timeStampOfRating;
	private ObservableList<Rating> ratingsOfApp;

	public RatingsViewController(final RatingsApi ratingsApi,
	                             final AppsApi appsApi,
	                             final AppsViewController appsViewController,
	                             final CompanyViewController companyViewController,
	                             final FxWeaver fxWeaver) {
		this.ratingsApi = ratingsApi;
		this.appsApi = appsApi;
		this.appsViewController = appsViewController;
		this.companyViewController = companyViewController;
		this.fxWeaver = fxWeaver;
	}

	@FXML
	public void initialize() throws ApiException {
		List<Rating> extractedRatings =
				ratingsApi.apiV1AppsAppIdRatingsGet(appsViewController.getCurrentlySelectedApp().getId());
		ratingsOfApp = FXCollections.observableList(extractedRatings);
		ratingTable.setItems(ratingsOfApp);
		ratedApp.setCellValueFactory(CallbackFactory.createTableCellValueFactory(rating-> {
			try {
				return appsApi.getApp(rating.getAppId());
			}
			catch (ApiException e) {
				e.printStackTrace();
			}
			return null;
		}));
		author.setCellValueFactory(CallbackFactory.createTableCellValueFactory(Rating::getAuthor));
		ratingStars.setCellValueFactory(CallbackFactory.createTableCellValueFactory(Rating::getStars));
		timeStampOfRating.setCellValueFactory(CallbackFactory.createTableCellValueFactory(rating->{
				Calendar calendar = GregorianCalendar.getInstance();
				calendar.setTimeInMillis(rating.getTimestamp()*1000);
				return calendar.getTime().toString();
		}));
	}

	public void onBtnDeleteRating(final MouseEvent mouseEvent) throws ApiException {
		new Thread(()-> {
			if (getCurrentlySelectedRating() == null) {
				JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
				return;
			}

			try {
				ratingsApi.apiV1AppsAppIdRatingsIdDelete(getCurrentlySelectedRating().getAppId(),
						getCurrentlySelectedRating().getId());
			}
			catch (ApiException e) {
				e.printStackTrace();
			}
			ratingsOfApp.remove(getCurrentlySelectedRating());
		}).start();
	}

	public void onBtnAddRating(final MouseEvent mouseEvent) throws IOException {
		final CreateEditRatingDialog dialog = new CreateEditRatingDialog(fxWeaver,
				companyViewController);
		dialog.openCreate();
	}

	public void onBtnReturnToApp(final MouseEvent mouseEvent) {
		Parent root = fxWeaver.loadView(AppsViewController.class);
		Scene scene = new Scene(root, companyViewController.getCorrectWidth(),
				companyViewController.getCorrectHeight());
		companyViewController.getStage().setScene(scene);
	}

	public List<Rating> getRatingsOfApp() {
		return ratingsOfApp;
	}

	public Rating getCurrentlySelectedRating(){
		return ratingTable.getSelectionModel().getSelectedItem();
	}
}
