package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.AppsApi;
import de.stl.saar.client.api.RatingsApi;
import de.stl.saar.client.model.App;
import de.stl.saar.client.model.Rating;
import de.stl.saar.client.model.RatingPostRequest;
import de.stl.saar.exceptions.GlobalException;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

@Component
@FxmlView("createRating.fxml")
public class CreateRatingController {
	private final RatingsApi ratingsApi;
	private final RatingsViewController ratingsViewController;
	private final AppsViewController appsViewController;
	private final AppsApi appsApi;
	public TextField authorName;
	public TextField ratingStars;
	public TextArea ratingText;
	public ComboBox<App> appToBeRated;
	private Stage stage;

	public CreateRatingController(final RatingsApi ratingsApi,
	                              final RatingsViewController ratingsViewController,
	                              final AppsViewController appsViewController,
	                              final AppsApi appsApi) {
		this.ratingsApi = ratingsApi;
		this.ratingsViewController = ratingsViewController;
		this.appsViewController = appsViewController;
		this.appsApi = appsApi;
	}

	@FXML
	public void initialize() throws ApiException {
		appToBeRated.setItems(FXCollections.observableList(appsApi.apiV1AppsGet(Integer.MAX_VALUE,0)));
		appToBeRated.setValue(appsViewController.getCurrentlySelectedApp());
		ratingStars.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				ratingStars.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});
	}
	public void setStage(final Stage stage) {
		this.stage = stage;
	}

	public void btnSaveRating(final MouseEvent mouseEvent) throws ApiException {
		try {
			App selectedApp = appToBeRated.getSelectionModel().getSelectedItem();
			if (selectedApp == null) {
				JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(),
						ExceptionUtils.getNoItemSelectedDetailed());
				return;
			}
			if(ratingStars.getText().isBlank()){
				return;
			}
			final RatingPostRequest ratingPostRequest =
					new RatingPostRequest(selectedApp.getId(), authorName.getText(),
							ratingText.getText(),
							Integer.valueOf(ratingStars.getText()));
			ExceptionUtils.validateRatingPostRequest(ratingPostRequest);
			final Rating savedRating =
					ratingsApi.apiV1AppsAppIdRatingsPost(selectedApp.getId(), ratingPostRequest);
				ratingsViewController.getRatingsOfApp().add(savedRating);
				stage.close();
		}
		catch (GlobalException globalException) {
			JavaFXUtils.showErrorMessage(GlobalException.header, globalException.getMessage());
		}
	}
}
