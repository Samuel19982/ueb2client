package de.stl.saar.view.controller;


import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.AppsApi;
import de.stl.saar.client.api.VersionsApi;
import de.stl.saar.client.model.App;
import de.stl.saar.client.model.AppVersion;
import de.stl.saar.client.model.AppVersionPostRequest;
import de.stl.saar.exceptions.GlobalException;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

@Component
@FxmlView("createVersion.fxml")
public class CreateVersionController {
	private final VersionsApi versionsApi;
	private final AppsViewController appsViewController;
	private final VersionsViewController versionsViewController;
	private final AppsApi appsApi;
	public TextField version;
	public TextField releseNotes;
	public TextField blobURL;
	public ComboBox<App> containingApp;
	private ObservableList<App> apps;
	private Stage stage;

	public CreateVersionController(final VersionsApi versionsApi,
	                               final AppsViewController appsViewController,
	                               final VersionsViewController versionsViewController,
	                               final AppsApi appsApi) {
		this.versionsApi = versionsApi;
		this.appsViewController = appsViewController;
		this.versionsViewController = versionsViewController;
		this.appsApi = appsApi;
	}

	@FXML
	public void initialize() throws ApiException {
		apps = FXCollections.observableList(appsApi.apiV1AppsGet(Integer.MAX_VALUE,0));
		containingApp.setItems(apps);
		containingApp.setValue(appsViewController.getCurrentlySelectedApp());
	}

	public void btnSaveVersion(final MouseEvent mouseEvent) throws ApiException {
		try{
		AppVersionPostRequest appVersionPostRequest =
				new AppVersionPostRequest(containingApp.getSelectionModel().getSelectedItem().getId(),
						version.getText(), releseNotes.getText(),blobURL.getText());
			ExceptionUtils.validateAppVersionPostRequest(appVersionPostRequest);
		AppVersion appVersion =
				versionsApi.apiV1AppsAppIdVersionsPost(containingApp.getSelectionModel().getSelectedItem().getId(),
				appVersionPostRequest);
		if(containingApp.getSelectionModel().getSelectedItem().getId().equals(appsViewController.getCurrentlySelectedApp().getId())){
			versionsViewController.getAppVersionObservableList().add(appVersion);
		}
		stage.close();
		}
		catch (final GlobalException globalException){
			JavaFXUtils.showErrorMessage(GlobalException.header, globalException.getMessage());
		}
	}

	public void setStage(final Stage stage) {
		this.stage = stage;
	}

	public void editVersion(final AppVersion appVersion) {

	}
}
