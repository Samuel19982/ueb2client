package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.AppsApi;
import de.stl.saar.client.api.CompaniesApi;
import de.stl.saar.client.model.App;
import de.stl.saar.client.model.AppPostRequest;
import de.stl.saar.client.model.Company;
import de.stl.saar.exceptions.GlobalException;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

@Component
@FxmlView("createApp.fxml")
public class CreateAppController {
	private final CompanyViewController companyViewController;
	private final AppsViewController appsViewController;
	private final CompaniesApi companiesApi;
	private final AppsApi appsApi;
	public TextField appTitle;
	public TextField appDescription;
	public TextField appPackage;
	public ComboBox<Company> developingCompany;
	public Label title;
	private ObservableList<Company> availableCompanies;
	private Stage stage;
	private boolean isEditing = false;
	private App editingApp;

	public CreateAppController(final CompanyViewController companyViewController,
	                           final AppsViewController appsViewController,
	                           final CompaniesApi companiesApi, final AppsApi appsApi) {
		this.companyViewController = companyViewController;
		this.appsViewController = appsViewController;
		this.companiesApi = companiesApi;
		this.appsApi = appsApi;
	}

	@FXML
	public void initialize() throws ApiException {
		if(isEditing){
			title.setText("App bearbeiten");
		}
		availableCompanies = FXCollections.observableArrayList(companiesApi.apiV1CompaniesGet());
		developingCompany.setItems(availableCompanies);
		developingCompany.setValue(companyViewController.getCurrentlySelectedCompany());
	}

	public void btnSaveApp(final MouseEvent mouseEvent) throws ApiException {
		try{
			final String newDevelopingCompanyId = developingCompany.getSelectionModel().getSelectedItem().getId();
			final App savedApp;
			if (!isEditing) {
				AppPostRequest appPostRequest = new AppPostRequest(appTitle.getText(), appDescription.getText(),
						appPackage.getText(), newDevelopingCompanyId);
				ExceptionUtils.validateAppPostRequest(appPostRequest);
				savedApp = appsApi.apiV1AppsPost(appPostRequest);

			}
			else {
				editingApp.setTitle(appTitle.getText());
				editingApp.setCompanyId(newDevelopingCompanyId);
				editingApp.setDescription(appDescription.getText());
				editingApp.setPackage(appPackage.getText());
				appsApi.apiV1AppsIdPut(editingApp, editingApp.getId());
				savedApp = editingApp;

			}
			//Wenn selectedCompany und das zu entwickelnde Unternehmen gleich sind, so müssen wir es zusätzlich in der
			// View anpassen
			final Company selectedCompany =
					companyViewController.getCompanyTable().getSelectionModel().getSelectedItem();
			if (selectedCompany.getId().equals(newDevelopingCompanyId) && !isEditing) {
				appsViewController.getAppsObservableList().add(savedApp);
			}
			else {
				final int index = appsViewController.determineIndexInList(editingApp.getId());
				appsViewController.updateElement(index, editingApp);
			}
			stage.close();
		}
		catch (GlobalException globalException){
			JavaFXUtils.showErrorMessage(GlobalException.header, globalException.getMessage());
		}
	}

	public void setStage(final Stage stage) {
		this.stage = stage;
	}

	public void editApp(final App currentlySelectedApp) {
		developingCompany.setValue(companyViewController.getCurrentlySelectedCompany());
		appTitle.setText(currentlySelectedApp.getTitle());
		appDescription.setText(currentlySelectedApp.getDescription());
		appPackage.setText(currentlySelectedApp.getPackage());
		isEditing = true;
		editingApp = currentlySelectedApp;
	}
}
