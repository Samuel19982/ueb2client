package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.VersionsApi;
import de.stl.saar.client.model.AppVersion;
import de.stl.saar.factories.CallbackFactory;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import de.stl.saar.view.dialog.CreateEditVersionDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import net.rgielen.fxweaver.core.FxWeaver;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;

@Component
@FxmlView("versionsView.fxml")
public class VersionsViewController {
	private final FxWeaver fxWeaver;
	private final CompanyViewController companyViewController;
	private final VersionsApi versionsApi;
	private final AppsViewController appsViewController;
	public TableColumn<AppVersion, String> versionTitle;
	public TableColumn<AppVersion, String> versionURL;
	public TableColumn<AppVersion, String> versionReleaseNotes;
	public TableView<AppVersion> versionTable;
	private ObservableList<AppVersion> appVersionObservableList;

	public VersionsViewController(final FxWeaver fxWeaver,
	                              final CompanyViewController companyViewController,
	                              final VersionsApi versionsApi,
	                              final AppsViewController appsViewController) {
		this.fxWeaver = fxWeaver;
		this.companyViewController = companyViewController;
		this.versionsApi = versionsApi;
		this.appsViewController = appsViewController;
		appVersionObservableList = FXCollections.observableList(new ArrayList<>());
	}

	@FXML
	public void initialize() throws ApiException {
		appVersionObservableList.clear();
		appVersionObservableList.addAll(versionsApi.apiV1AppsAppIdVersionsGet(appsViewController.getCurrentlySelectedApp().getId()));
		versionTable.setItems(appVersionObservableList);
		versionTitle.setCellValueFactory(CallbackFactory.createTableCellValueFactory(AppVersion::getVersion));
		versionURL.setCellValueFactory(CallbackFactory.createTableCellValueFactory(AppVersion::getBlobUrl));
		versionReleaseNotes.setCellValueFactory(CallbackFactory.createTableCellValueFactory(AppVersion::getReleaseNotes));

	}

	public void onTableViewVersionClicked(final MouseEvent mouseEvent) {
	}

	public void onBtnDeleteVersionClicked(final MouseEvent mouseEvent) throws ApiException {
		if(getCurrentlySelectedAppVersion()==null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}
		versionsApi.apiV1AppsAppIdVersionsIdDelete(getCurrentlySelectedAppVersion().getAppId(),
				getCurrentlySelectedAppVersion().getId());
		appVersionObservableList.remove(getCurrentlySelectedAppVersion());
	}

	public void onBtnAddVersionClicked(final MouseEvent mouseEvent) throws IOException {
		final CreateEditVersionDialog dialog = new CreateEditVersionDialog(fxWeaver,
				companyViewController);
		dialog.openCreate();
	}

	public void onBtnReturnToApp(final MouseEvent mouseEvent) {
		Parent root = fxWeaver.loadView(AppsViewController.class);
		Scene scene = new Scene(root, companyViewController.getCorrectWidth(),
				companyViewController.getCorrectHeight());
		companyViewController.getStage().setScene(scene);
	}

	public AppVersion getCurrentlySelectedAppVersion(){
		return versionTable.getSelectionModel().getSelectedItem();
	}

	public ObservableList<AppVersion> getAppVersionObservableList() {
		return appVersionObservableList;
	}

	public void onBtnShowScreenshots(final MouseEvent mouseEvent) {
		Parent root = fxWeaver.loadView(ScreenshotsViewController.class);
		Scene scene = new Scene(root, companyViewController.getCorrectWidth(),
				companyViewController.getCorrectHeight());
		companyViewController.getStage().setScene(scene);
	}
}
