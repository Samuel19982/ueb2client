package de.stl.saar.view.controller;


import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.ScreenshotsApi;
import de.stl.saar.client.model.AppVersion;
import de.stl.saar.client.model.Screenshot;
import de.stl.saar.factories.CallbackFactory;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import de.stl.saar.view.dialog.CreateEditScreenshotDialog;
import de.stl.saar.view.dialog.CreateEditVersionDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import net.rgielen.fxweaver.core.FxWeaver;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;

@Component
@FxmlView("screenshotsView.fxml")
public class ScreenshotsViewController {

	public TableView<Screenshot> screenshotTable;
	public TableColumn<Screenshot, String> url;
	public TableColumn<Screenshot, String> description;
	private final FxWeaver fxWeaver;
	private final CompanyViewController companyViewController;
	private final VersionsViewController versionsViewController;
	private ObservableList<Screenshot> screenshotsOfVersionAndApp;
	private final ScreenshotsApi screenshotsApi;

	public ScreenshotsViewController(final FxWeaver fxWeaver,
	                                 final CompanyViewController companyViewController,
	                                 final VersionsViewController versionsViewController,
	                                 final ScreenshotsApi screenshotsApi) {
		this.fxWeaver = fxWeaver;
		this.companyViewController = companyViewController;
		this.versionsViewController = versionsViewController;
		this.screenshotsApi = screenshotsApi;
		screenshotsOfVersionAndApp = FXCollections.observableList(new ArrayList<>());
	}

	@FXML
	public void initialize() throws ApiException {
		screenshotsOfVersionAndApp.clear();
		screenshotsOfVersionAndApp.addAll(
				screenshotsApi.apiV1AppsAppIdVersionsVersionIdScreenshotsGet(versionsViewController.getCurrentlySelectedAppVersion().getAppId(),
						versionsViewController.getCurrentlySelectedAppVersion().getId()));
		screenshotTable.setItems(screenshotsOfVersionAndApp);
		url.setCellValueFactory(CallbackFactory.createTableCellValueFactory(Screenshot::getUrl));
		description.setCellValueFactory(CallbackFactory.createTableCellValueFactory(Screenshot::getDescription));
	}

	public void onBtnReturnToVersions(final MouseEvent mouseEvent) {
		Parent root = fxWeaver.loadView(VersionsViewController.class);
		Scene scene = new Scene(root, companyViewController.getCorrectWidth(),
				companyViewController.getCorrectHeight());
		companyViewController.getStage().setScene(scene);
	}

	public void onBtnAddScreenshotClicked(final MouseEvent mouseEvent) throws IOException {
		final CreateEditScreenshotDialog dialog = new CreateEditScreenshotDialog(fxWeaver,
				companyViewController);
		dialog.openCreate();
	}

	public void onBtnDeleteScreenshotClicked(final MouseEvent mouseEvent) throws ApiException {
		if(getCurrentlySelectedScreenshot() ==null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}
		screenshotsApi.apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete(versionsViewController.getCurrentlySelectedAppVersion().getAppId()
				,versionsViewController.getCurrentlySelectedAppVersion().getId(),
				getCurrentlySelectedScreenshot().getId());
		screenshotsOfVersionAndApp.remove(getCurrentlySelectedScreenshot());
	}

	public void onTableViewVersionClicked(final MouseEvent mouseEvent) {
	}

	public Screenshot getCurrentlySelectedScreenshot(){
		return screenshotTable.getSelectionModel().getSelectedItem();
	}

	public ObservableList<Screenshot> getScreenshotsOfVersionAndApp() {
		return screenshotsOfVersionAndApp;
	}
}
