package de.stl.saar.view.controller;

import de.stl.saar.factories.CallbackFactory;
import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.CompaniesApi;
import de.stl.saar.client.model.Company;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import javafx.scene.Parent;
import javafx.scene.Scene;
import net.rgielen.fxweaver.core.FxWeaver;
import org.springframework.beans.factory.annotation.Autowired;
import de.stl.saar.view.dialog.CreateEditCompanyDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@FxmlView("companyView.fxml")
public class CompanyViewController {
	@FXML
	public TableView<Company> companyTable;
	@FXML
	public TableColumn<Company, String> companyCity;
	public TableColumn<Company, String> companyName;
	public TableColumn<Company, String> comanyStreet;
	public TableColumn<Company, String> companyCountry;
	public TableColumn<Company, String> companyZipCode;
	private Stage stage;
	private final SortedList<Company> sortedFilteredList;
	private final FilteredList<Company> companyFilteredList;
	private ObservableList<Company> companyObservableList;
	private final CompaniesApi companiesApi;
	private final FxWeaver fxWeaver;
	private ApplicationContext applicationContext;

	@Autowired
	public CompanyViewController(final CompaniesApi companiesApi, final FxWeaver fxWeaver) throws ApiException {
		this.companiesApi = companiesApi;
		this.fxWeaver = fxWeaver;
		companyObservableList = FXCollections.observableArrayList();
		companyFilteredList = new FilteredList<>(companyObservableList);
		sortedFilteredList = new SortedList<>(companyFilteredList);
	}

	@FXML
	public void initialize() throws ApiException {
		companyObservableList = FXCollections.observableArrayList(this.companiesApi.apiV1CompaniesGet());
		companyTable.setItems(companyObservableList);
		companyCity.setCellValueFactory(CallbackFactory.createTableCellValueFactory(company -> company.getAddress().getCity()));
		companyName.setCellValueFactory(CallbackFactory.createTableCellValueFactory(Company::getName));
		comanyStreet.setCellValueFactory(CallbackFactory.createTableCellValueFactory(company -> company.getAddress().getStreet()));
		companyCountry.setCellValueFactory(CallbackFactory.createTableCellValueFactory(company -> company.getAddress().getCountry()));
		companyZipCode.setCellValueFactory(CallbackFactory.createTableCellValueFactory(company -> company.getAddress().getPostalCode()));

	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(final Stage stage) {
		this.stage = stage;
	}

	public void onBtnDeleteCompanyClicked(final MouseEvent mouseEvent) throws ApiException {
		final Company selectedCompany = companyTable.getSelectionModel().getSelectedItem();
		if(selectedCompany!=null){
			companiesApi.apiV1CompaniesIdDelete(selectedCompany.getId());
			companyObservableList.remove(selectedCompany);
		}
	}

	public void onBtnAddCompanyClicked(final MouseEvent mouseEvent) throws IOException {
		final CreateEditCompanyDialog dialog = new CreateEditCompanyDialog(fxWeaver,
				this);
		dialog.openCreate();
	}

	public void onBtnShowAppsOfCompany(final MouseEvent mouseEvent) {
		if(getCurrentlySelectedCompany() ==null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}
		Parent root = fxWeaver.loadView(AppsViewController.class);
		Scene scene = new Scene(root, stage.getScene().getWidth(), stage.getScene().getHeight());
		stage.setScene(scene);
	}

	public double getCorrectHeight(){
		return stage.getScene().getHeight();
	}

	public double getCorrectWidth(){
		return stage.getScene().getWidth();
	}

	public ObservableList<Company> getCompanyObservableList() {
		return companyObservableList;
	}

	public void updateElement(int index, Company updatedCompany){
		companyObservableList.set(index, updatedCompany);
	}

	public int determineIndexInList(final String id){
		int counter=0;
		for (final Company company: companyObservableList){
			if(company.getId().equals(id)){
				return counter;
			}
			else{
				counter++;
			}
		}
		return -1;
	}

	public TableView<Company> getCompanyTable() {
		return companyTable;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public Company getCurrentlySelectedCompany(){
		return companyTable.getSelectionModel().getSelectedItem();
	}

	public void onTableViewCompanyClicked(final MouseEvent mouseEvent) throws IOException {
		if (mouseEvent.getClickCount() > 1 ) {
			final CreateEditCompanyDialog dialog = new CreateEditCompanyDialog(fxWeaver,
					this);
			dialog.openEdit(getCurrentlySelectedCompany());
		}
	}
}
