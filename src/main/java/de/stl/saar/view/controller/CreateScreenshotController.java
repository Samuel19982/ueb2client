package de.stl.saar.view.controller;

import de.stl.saar.client.ApiException;
import de.stl.saar.client.api.ScreenshotsApi;
import de.stl.saar.client.api.VersionsApi;
import de.stl.saar.client.model.AppVersion;
import de.stl.saar.client.model.Screenshot;
import de.stl.saar.client.model.ScreenshotPostRequest;
import de.stl.saar.exceptions.GlobalException;
import de.stl.saar.utils.ExceptionUtils;
import de.stl.saar.utils.JavaFXUtils;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

@Component
@FxmlView("createScreenshot.fxml")
public class CreateScreenshotController {
	private final ScreenshotsApi screenshotsApi;
	private final VersionsApi versionsApi;
	private final VersionsViewController versionsViewController;
	private final ScreenshotsViewController screenshotsViewController;
	public TextField url;
	public TextField description;
	public ComboBox<AppVersion> versionToBeAddedToSelector;
	private Stage stage;

	public CreateScreenshotController(final ScreenshotsApi screenshotsApi,
	                                  final VersionsApi versionsApi,
	                                  final VersionsViewController versionsViewController,
	                                  final ScreenshotsViewController screenshotsViewController) {
		this.screenshotsApi = screenshotsApi;
		this.versionsApi = versionsApi;
		this.versionsViewController = versionsViewController;
		this.screenshotsViewController = screenshotsViewController;
	}

	@FXML
	public void initialize() throws ApiException {
		versionToBeAddedToSelector.setItems(FXCollections.observableList(versionsApi.apiV1AppsAppIdVersionsGet(versionsViewController.getCurrentlySelectedAppVersion().getAppId())));
	}

	public void btnSaveScreenshot(final MouseEvent mouseEvent) throws ApiException {
		try{
		final AppVersion versionToBeAddedTo = versionToBeAddedToSelector.getSelectionModel().getSelectedItem();
		if( versionToBeAddedTo == null){
			JavaFXUtils.showErrorMessage(ExceptionUtils.getNoItemSelected(), ExceptionUtils.getNoItemSelectedDetailed());
			return;
		}
		final ScreenshotPostRequest screenshotPostRequest = new ScreenshotPostRequest(versionToBeAddedTo.getId(),
				url.getText(), description.getText());
		ExceptionUtils.validateScreenshotPostRequest(screenshotPostRequest);
		final Screenshot savedScreenshot =
				screenshotsApi.apiV1AppsAppIdVersionsVersionIdScreenshotsPost(versionToBeAddedTo.getAppId(),
				versionToBeAddedTo.getId(),screenshotPostRequest);
		screenshotsViewController.getScreenshotsOfVersionAndApp().add(savedScreenshot);
		stage.close();
	}catch (GlobalException globalException){
		JavaFXUtils.showErrorMessage(GlobalException.header, globalException.getMessage());
		}
	}

	public void setStage(final Stage stage) {
		this.stage = stage;
	}
}
