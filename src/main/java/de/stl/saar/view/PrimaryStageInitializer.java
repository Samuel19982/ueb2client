package de.stl.saar.view;

import de.stl.saar.constants.FXConstants;
import de.stl.saar.view.controller.CompanyViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class PrimaryStageInitializer implements ApplicationListener<StageReadyEvent> {
	private final FxWeaver fxWeaver;
	private static Parent root;

	@Autowired
	public PrimaryStageInitializer(final FxWeaver fxWeaver) {
		this.fxWeaver = fxWeaver;
	}

	@Override
	public void onApplicationEvent(final StageReadyEvent event) {
		Stage stage = event.stage;
		FXMLLoader fxmlLoader = new FXMLLoader(CompanyViewController.class.getResource(FXConstants.PATH_COMPANY_VIEW));
		//FxManagerController.class   fxManager
		try {
			root = fxmlLoader.load();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		CompanyViewController fxManagerController=fxmlLoader.getController();

		try {
			stage.setScene(fxmlLoader.load());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		stage.setTitle("Appstore");
		stage.setMinHeight(605);
		stage.setMinWidth(1200);
		stage.setResizable(true);
		stage.centerOnScreen();
		stage.show();
	}
}
