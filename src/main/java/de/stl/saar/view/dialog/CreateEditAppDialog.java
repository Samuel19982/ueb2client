package de.stl.saar.view.dialog;

import de.stl.saar.client.model.App;
import de.stl.saar.view.controller.CompanyViewController;
import de.stl.saar.view.controller.CreateAppController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;

import java.io.IOException;

public class CreateEditAppDialog {
	private final Stage stage;
	private final CompanyViewController companyViewController;
	private final CreateAppController createAppController;

	public CreateEditAppDialog(final FxWeaver fxWeaver,
	                           final CompanyViewController companyViewController) throws IOException {
		this.companyViewController = companyViewController;
		stage = new Stage();
		//stage.getIcons().add(FXUtils.getImageIcon());

		Parent root = fxWeaver.loadView(CreateAppController.class);
		Scene scene = new Scene(root);
		createAppController =
				companyViewController.getApplicationContext().getBean(CreateAppController.class);
		createAppController.setStage(stage);
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
	}

	public void openCreate() {
		stage.setTitle("App erstellen");
		stage.setMinHeight(393);
		stage.setMinWidth(657);
		stage.showAndWait();
	}

	public void openEdit(final App currentlySelectedApp) {
		createAppController.editApp(currentlySelectedApp);
		stage.setTitle("App bearbeiten");
		stage.setMinHeight(605);
		stage.setMinWidth(680);
		stage.showAndWait();
	}
}
