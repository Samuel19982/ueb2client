package de.stl.saar.view.dialog;

import de.stl.saar.client.model.App;
import de.stl.saar.client.model.AppVersion;
import de.stl.saar.view.controller.CompanyViewController;
import de.stl.saar.view.controller.CreateAppController;
import de.stl.saar.view.controller.CreateVersionController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;

import java.io.IOException;

public class CreateEditVersionDialog {
	private final CompanyViewController companyViewController;
	private final CreateVersionController createVersionController;
	private final Stage stage;


	public CreateEditVersionDialog(final FxWeaver fxWeaver,
	                               final CompanyViewController companyViewController) throws IOException {
		this.companyViewController = companyViewController;
		stage = new Stage();
		//stage.getIcons().add(FXUtils.getImageIcon());

		Parent root = fxWeaver.loadView(CreateVersionController.class);
		Scene scene = new Scene(root);
		createVersionController =
				companyViewController.getApplicationContext().getBean(CreateVersionController.class);
		createVersionController.setStage(stage);
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
	}

	public void openCreate() {
		stage.setTitle("Version erstellen");
		stage.setMinHeight(393);
		stage.setMinWidth(657);
		stage.showAndWait();
	}

	public void openEdit(final AppVersion appVersion) {
		createVersionController.editVersion(appVersion);
		stage.setTitle("Version bearbeiten");
		stage.setMinHeight(605);
		stage.setMinWidth(680);
		stage.showAndWait();
	}
}
