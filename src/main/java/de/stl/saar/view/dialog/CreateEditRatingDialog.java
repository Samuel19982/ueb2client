package de.stl.saar.view.dialog;

import de.stl.saar.view.controller.CompanyViewController;
import de.stl.saar.view.controller.CreateCompanyController;
import de.stl.saar.view.controller.CreateRatingController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;

import java.io.IOException;

public class CreateEditRatingDialog {
	private final Stage stage;

	public CreateEditRatingDialog(final FxWeaver fxWeaver,
	                               final CompanyViewController companyViewController) throws IOException {
		stage = new Stage();
		//stage.getIcons().add(FXUtils.getImageIcon());

		Parent root = fxWeaver.loadView(CreateRatingController.class);
		CreateRatingController createCompanyController =
				companyViewController.getApplicationContext().getBean(CreateRatingController.class);
		createCompanyController.setStage(stage);

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
	}

	/**
	 * Wird aufgerufen, wenn eine neue Firma erstellt werden soll.
	 */
	public void openCreate() {
		stage.setTitle("Bewertung erstellen");
		stage.setMinHeight(393);
		stage.setMinWidth(657);
		stage.showAndWait();
	}
}
