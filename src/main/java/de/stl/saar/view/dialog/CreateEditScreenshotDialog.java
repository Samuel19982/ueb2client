package de.stl.saar.view.dialog;

import de.stl.saar.view.controller.CompanyViewController;
import de.stl.saar.view.controller.CreateRatingController;
import de.stl.saar.view.controller.CreateScreenshotController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;

import java.io.IOException;

public class CreateEditScreenshotDialog {
	private final Stage stage;
	private final CreateScreenshotController createScreenshotController;

	public CreateEditScreenshotDialog(final FxWeaver fxWeaver,
	                              final CompanyViewController companyViewController) throws IOException {
		stage = new Stage();

		Parent root = fxWeaver.loadView(CreateScreenshotController.class);
		createScreenshotController =
				companyViewController.getApplicationContext().getBean(CreateScreenshotController.class);
		createScreenshotController.setStage(stage);

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
	}

	/**
	 * Wird aufgerufen, wenn eine neue Firma erstellt werden soll.
	 */
	public void openCreate() {
		stage.setTitle("Screenshot erstellen");
		stage.setMinHeight(393);
		stage.setMinWidth(657);
		stage.showAndWait();
	}
}
