package de.stl.saar.view.dialog;

import de.stl.saar.client.model.Company;
import de.stl.saar.view.controller.CompanyViewController;
import de.stl.saar.view.controller.CreateCompanyController;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

public class CreateEditCompanyDialog {
	private final Stage stage;
	private final CompanyViewController companyViewController;
	private final CreateCompanyController createCompanyController;

	public CreateEditCompanyDialog(final FxWeaver fxWeaver,
	                               final CompanyViewController companyViewController) throws IOException {
		this.companyViewController = companyViewController;
		stage = new Stage();
		//stage.getIcons().add(FXUtils.getImageIcon());

		Parent root = fxWeaver.loadView(CreateCompanyController.class);
		createCompanyController =
				companyViewController.getApplicationContext().getBean(CreateCompanyController.class);
		createCompanyController.setStage(stage);

		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.initModality(Modality.APPLICATION_MODAL);
	}

	/**
	 * Wird aufgerufen, wenn eine neue Firma erstellt werden soll.
	 */
	public void openCreate() {
		stage.setTitle("Unternehmen erstellen");
		stage.setMinHeight(393);
		stage.setMinWidth(657);
		stage.showAndWait();
	}

	/**
	 * Wird aufgerufen, wenn eine  Karte bearbeitet werden soll.
	 * @param companyToEdit das zu bearbeitende Unternehmen
	 */
	public void openEdit(Company companyToEdit) {
		createCompanyController.editCompany(companyToEdit);
		stage.setTitle("Unternehmen bearbeiten");
		stage.setMinHeight(605);
		stage.setMinWidth(680);
		stage.showAndWait();
	}
}
