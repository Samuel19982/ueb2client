package de.stl.saar.view;

import com.dustinredmond.fxtrayicon.FXTrayIcon;
import de.stl.saar.StartSwaggerClient;
import de.stl.saar.view.controller.CompanyViewController;
import de.stl.saar.view.controller.SelectConnectionController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

public class MainApplication extends Application {
	private static Stage mainStage;
	private ConfigurableApplicationContext context;


	private ConfigurableApplicationContext applicationContext;

	@Override
	public void init() {
		String[] args = getParameters().getRaw().toArray(new String[0]);
		this.applicationContext = new SpringApplicationBuilder()
				.sources(StartSwaggerClient.class)
				.run(args);
	}

	@Override
	public void start(Stage stage) {
		FxWeaver fxWeaver = applicationContext.getBean(FxWeaver.class);
		Parent root = fxWeaver.loadView(SelectConnectionController.class);
		Scene scene = new Scene(root);
		stage.setMaximized(true);
		stage.setScene(scene);
		stage.setOnCloseRequest(e->stop());
		SelectConnectionController companyViewController = applicationContext.getBean(SelectConnectionController.class);
		companyViewController.setStage(stage);
		companyViewController.setApplicationContext(applicationContext);
		stage.show();
	}

	@Override
	public void stop() {
		this.applicationContext.close();
		Platform.exit();
		System.out.println("Geschlossen");
		System.exit(0);
	}
}
