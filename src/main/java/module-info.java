module swagger.client {
	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires spring.context;
	requires com.google.gson;
	requires okio;
	requires org.threeten.bp;
	requires java.annotation;
	requires lombok;
	requires org.slf4j;
	requires io.swagger.v3.oas.annotations;
	requires java.validation;
	requires net.rgielen.fxweaver.core;
	requires spring.beans;
	requires spring.boot.autoconfigure;
	requires spring.boot;
	requires spring.core;
	requires FXTrayIcon;
	requires java.net.http;
	requires okhttp3;
	requires okhttp3.logging;
	requires annotations;
	requires kotlin.stdlib;

	opens de.stl.saar.view;
	opens de.stl.saar;
	opens de.stl.saar.client;
	opens de.stl.saar.client.api;
	opens de.stl.saar.view.controller;
	opens de.stl.saar.client.model;

	exports de.stl.saar.view;
	exports de.stl.saar.utils;
	exports de.stl.saar.client;
	exports de.stl.saar.client.api;
	exports de.stl.saar.view.controller;
	exports de.stl.saar.client.model;
}