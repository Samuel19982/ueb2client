package de.stl.saar.client.api

import de.stl.saar.client.model.AppPostRequest
import spock.lang.Specification

class AppPostRequestTest extends Specification{

    def "should test constructor"(){
        given:
        AppPostRequest appPostRequest;

        when:
            appPostRequest = AppPostRequest
                    .builder()
                    .title("Test")
                    ._package("Testpackage")
                    .companyId("1235")
                    .description("test")
                    .build()

        then:
            appPostRequest.getTitle() == "Test"

    }
}
