# CompanyPostRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**address** | [**Address**](Address.md) |  | 
