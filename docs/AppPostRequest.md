# AppPostRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**description** | **String** |  |  [optional]
**_package** | **String** |  | 
**companyId** | **String** |  | 
