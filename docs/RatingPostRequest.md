# RatingPostRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** |  | 
**author** | **String** |  | 
**text** | **String** |  | 
**timestamp** | **Integer** |  | 
**stars** | **Integer** |  | 
