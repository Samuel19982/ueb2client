# CompaniesApi

All URIs are relative to *https://appstore.education.cl2.smef.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1CompaniesGet**](CompaniesApi.md#apiV1CompaniesGet) | **GET** /api/v1/Companies | Returns all companies
[**apiV1CompaniesIdDelete**](CompaniesApi.md#apiV1CompaniesIdDelete) | **DELETE** /api/v1/Companies/{id} | Deletes the addresses company
[**apiV1CompaniesIdGet**](CompaniesApi.md#apiV1CompaniesIdGet) | **GET** /api/v1/Companies/{id} | Returns the addresses company
[**apiV1CompaniesIdPut**](CompaniesApi.md#apiV1CompaniesIdPut) | **PUT** /api/v1/Companies/{id} | Updates a company object
[**apiV1CompaniesPost**](CompaniesApi.md#apiV1CompaniesPost) | **POST** /api/v1/Companies | Creates an new company

<a name="apiV1CompaniesGet"></a>
# **apiV1CompaniesGet**
> List&lt;Company&gt; apiV1CompaniesGet()

Returns all companies

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.CompaniesApi;


CompaniesApi apiInstance = new CompaniesApi();
try {
    List<Company> result = apiInstance.apiV1CompaniesGet();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompaniesApi#apiV1CompaniesGet");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Company&gt;**](Company.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1CompaniesIdDelete"></a>
# **apiV1CompaniesIdDelete**
> Company apiV1CompaniesIdDelete(id)

Deletes the addresses company

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.CompaniesApi;


CompaniesApi apiInstance = new CompaniesApi();
String id = "id_example"; // String | the id of the company
try {
    Company result = apiInstance.apiV1CompaniesIdDelete(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompaniesApi#apiV1CompaniesIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the company |

### Return type

[**Company**](Company.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1CompaniesIdGet"></a>
# **apiV1CompaniesIdGet**
> Company apiV1CompaniesIdGet(id)

Returns the addresses company

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.CompaniesApi;


CompaniesApi apiInstance = new CompaniesApi();
String id = "id_example"; // String | the id of the company
try {
    Company result = apiInstance.apiV1CompaniesIdGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompaniesApi#apiV1CompaniesIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the company |

### Return type

[**Company**](Company.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1CompaniesIdPut"></a>
# **apiV1CompaniesIdPut**
> apiV1CompaniesIdPut(id, body)

Updates a company object

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.CompaniesApi;


CompaniesApi apiInstance = new CompaniesApi();
String id = "id_example"; // String | the id of the company
Company body = new Company(); // Company | the changed company object
try {
    apiInstance.apiV1CompaniesIdPut(id, body);
} catch (ApiException e) {
    System.err.println("Exception when calling CompaniesApi#apiV1CompaniesIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the company |
 **body** | [**Company**](Company.md)| the changed company object | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

<a name="apiV1CompaniesPost"></a>
# **apiV1CompaniesPost**
> Company apiV1CompaniesPost(body)

Creates an new company

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.CompaniesApi;


CompaniesApi apiInstance = new CompaniesApi();
CompanyPostRequest body = new CompanyPostRequest(); // CompanyPostRequest | the company object
try {
    Company result = apiInstance.apiV1CompaniesPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompaniesApi#apiV1CompaniesPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CompanyPostRequest**](CompanyPostRequest.md)| the company object | [optional]

### Return type

[**Company**](Company.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

