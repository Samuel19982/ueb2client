# ScreenshotPostRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**versionId** | **String** |  | 
**url** | **String** |  | 
**description** | **String** |  |  [optional]
