# VersionsApi

All URIs are relative to *https://appstore.education.cl2.smef.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1AppsAppIdVersionsGet**](VersionsApi.md#apiV1AppsAppIdVersionsGet) | **GET** /api/v1/Apps/{appId}/Versions | returns all versions of an app
[**apiV1AppsAppIdVersionsIdDelete**](VersionsApi.md#apiV1AppsAppIdVersionsIdDelete) | **DELETE** /api/v1/Apps/{appId}/Versions/{id} | Deletes a version of an app
[**apiV1AppsAppIdVersionsIdGet**](VersionsApi.md#apiV1AppsAppIdVersionsIdGet) | **GET** /api/v1/Apps/{appId}/Versions/{id} | Returns a version of an app
[**apiV1AppsAppIdVersionsPost**](VersionsApi.md#apiV1AppsAppIdVersionsPost) | **POST** /api/v1/Apps/{appId}/Versions | Creates a new version for an app

<a name="apiV1AppsAppIdVersionsGet"></a>
# **apiV1AppsAppIdVersionsGet**
> List&lt;AppVersion&gt; apiV1AppsAppIdVersionsGet(appId)

returns all versions of an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.VersionsApi;


VersionsApi apiInstance = new VersionsApi();
String appId = "appId_example"; // String | the id of the app
try {
    List<AppVersion> result = apiInstance.apiV1AppsAppIdVersionsGet(appId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV1AppsAppIdVersionsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |

### Return type

[**List&lt;AppVersion&gt;**](AppVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdVersionsIdDelete"></a>
# **apiV1AppsAppIdVersionsIdDelete**
> apiV1AppsAppIdVersionsIdDelete(appId, id)

Deletes a version of an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.VersionsApi;


VersionsApi apiInstance = new VersionsApi();
String appId = "appId_example"; // String | the id of the app
String id = "id_example"; // String | the id of the version
try {
    apiInstance.apiV1AppsAppIdVersionsIdDelete(appId, id);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV1AppsAppIdVersionsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **id** | **String**| the id of the version |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdVersionsIdGet"></a>
# **apiV1AppsAppIdVersionsIdGet**
> AppVersion apiV1AppsAppIdVersionsIdGet(appId, id)

Returns a version of an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.VersionsApi;


VersionsApi apiInstance = new VersionsApi();
String appId = "appId_example"; // String | the id of the app
String id = "id_example"; // String | the id of the version
try {
    AppVersion result = apiInstance.apiV1AppsAppIdVersionsIdGet(appId, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV1AppsAppIdVersionsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **id** | **String**| the id of the version |

### Return type

[**AppVersion**](AppVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdVersionsPost"></a>
# **apiV1AppsAppIdVersionsPost**
> AppVersion apiV1AppsAppIdVersionsPost(appId, body)

Creates a new version for an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.VersionsApi;


VersionsApi apiInstance = new VersionsApi();
String appId = "appId_example"; // String | the id of the app
AppVersionPostRequest body = new AppVersionPostRequest(); // AppVersionPostRequest | the version to create
try {
    AppVersion result = apiInstance.apiV1AppsAppIdVersionsPost(appId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VersionsApi#apiV1AppsAppIdVersionsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **body** | [**AppVersionPostRequest**](AppVersionPostRequest.md)| the version to create | [optional]

### Return type

[**AppVersion**](AppVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

