# RatingsApi

All URIs are relative to *https://appstore.education.cl2.smef.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1AppsAppIdRatingsGet**](RatingsApi.md#apiV1AppsAppIdRatingsGet) | **GET** /api/v1/Apps/{appId}/Ratings | Returns all ratings of an app
[**apiV1AppsAppIdRatingsIdDelete**](RatingsApi.md#apiV1AppsAppIdRatingsIdDelete) | **DELETE** /api/v1/Apps/{appId}/Ratings/{id} | Deletes a rating of an app
[**apiV1AppsAppIdRatingsPost**](RatingsApi.md#apiV1AppsAppIdRatingsPost) | **POST** /api/v1/Apps/{appId}/Ratings | Creates a new rating for an app

<a name="apiV1AppsAppIdRatingsGet"></a>
# **apiV1AppsAppIdRatingsGet**
> List&lt;Rating&gt; apiV1AppsAppIdRatingsGet(appId)

Returns all ratings of an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.RatingsApi;


RatingsApi apiInstance = new RatingsApi();
String appId = "appId_example"; // String | the id of the app
try {
    List<Rating> result = apiInstance.apiV1AppsAppIdRatingsGet(appId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RatingsApi#apiV1AppsAppIdRatingsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |

### Return type

[**List&lt;Rating&gt;**](Rating.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdRatingsIdDelete"></a>
# **apiV1AppsAppIdRatingsIdDelete**
> apiV1AppsAppIdRatingsIdDelete(appId, id)

Deletes a rating of an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.RatingsApi;


RatingsApi apiInstance = new RatingsApi();
String appId = "appId_example"; // String | the id of the app
String id = "id_example"; // String | the id of the version
try {
    apiInstance.apiV1AppsAppIdRatingsIdDelete(appId, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RatingsApi#apiV1AppsAppIdRatingsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **id** | **String**| the id of the version |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdRatingsPost"></a>
# **apiV1AppsAppIdRatingsPost**
> Rating apiV1AppsAppIdRatingsPost(appId, body)

Creates a new rating for an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.RatingsApi;


RatingsApi apiInstance = new RatingsApi();
String appId = "appId_example"; // String | the id of the app
RatingPostRequest body = new RatingPostRequest(); // RatingPostRequest | the rating to create
try {
    Rating result = apiInstance.apiV1AppsAppIdRatingsPost(appId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RatingsApi#apiV1AppsAppIdRatingsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **body** | [**RatingPostRequest**](RatingPostRequest.md)| the rating to create | [optional]

### Return type

[**Rating**](Rating.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

