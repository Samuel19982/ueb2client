# ScreenshotsApi

All URIs are relative to *https://appstore.education.cl2.smef.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1AppsAppIdVersionsVersionIdScreenshotsGet**](ScreenshotsApi.md#apiV1AppsAppIdVersionsVersionIdScreenshotsGet) | **GET** /api/v1/Apps/{appId}/Versions/{versionId}/Screenshots | Return screenshots of a version
[**apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete**](ScreenshotsApi.md#apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete) | **DELETE** /api/v1/Apps/{appId}/Versions/{versionId}/Screenshots/{id} | Deletes a screenshot
[**apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet**](ScreenshotsApi.md#apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet) | **GET** /api/v1/Apps/{appId}/Versions/{versionId}/Screenshots/{id} | Gets the addressed screenshot
[**apiV1AppsAppIdVersionsVersionIdScreenshotsPost**](ScreenshotsApi.md#apiV1AppsAppIdVersionsVersionIdScreenshotsPost) | **POST** /api/v1/Apps/{appId}/Versions/{versionId}/Screenshots | Create a screenshot for an appversion

<a name="apiV1AppsAppIdVersionsVersionIdScreenshotsGet"></a>
# **apiV1AppsAppIdVersionsVersionIdScreenshotsGet**
> List&lt;Screenshot&gt; apiV1AppsAppIdVersionsVersionIdScreenshotsGet(appId, versionId)

Return screenshots of a version

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.ScreenshotsApi;


ScreenshotsApi apiInstance = new ScreenshotsApi();
String appId = "appId_example"; // String | the id of the app
String versionId = "versionId_example"; // String | the id of the version
try {
    List<Screenshot> result = apiInstance.apiV1AppsAppIdVersionsVersionIdScreenshotsGet(appId, versionId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScreenshotsApi#apiV1AppsAppIdVersionsVersionIdScreenshotsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **versionId** | **String**| the id of the version |

### Return type

[**List&lt;Screenshot&gt;**](Screenshot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete"></a>
# **apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete**
> apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete(appId, versionId, id)

Deletes a screenshot

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.ScreenshotsApi;


ScreenshotsApi apiInstance = new ScreenshotsApi();
String appId = "appId_example"; // String | the id of the app
String versionId = "versionId_example"; // String | the id of the version
String id = "id_example"; // String | the id of the screenshot
try {
    apiInstance.apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete(appId, versionId, id);
} catch (ApiException e) {
    System.err.println("Exception when calling ScreenshotsApi#apiV1AppsAppIdVersionsVersionIdScreenshotsIdDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **versionId** | **String**| the id of the version |
 **id** | **String**| the id of the screenshot |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet"></a>
# **apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet**
> Screenshot apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet(appId, versionId, id)

Gets the addressed screenshot

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.ScreenshotsApi;


ScreenshotsApi apiInstance = new ScreenshotsApi();
String appId = "appId_example"; // String | the id of the app
String versionId = "versionId_example"; // String | the id of the version
String id = "id_example"; // String | the id of the screenshot
try {
    Screenshot result = apiInstance.apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet(appId, versionId, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScreenshotsApi#apiV1AppsAppIdVersionsVersionIdScreenshotsIdGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **versionId** | **String**| the id of the version |
 **id** | **String**| the id of the screenshot |

### Return type

[**Screenshot**](Screenshot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsAppIdVersionsVersionIdScreenshotsPost"></a>
# **apiV1AppsAppIdVersionsVersionIdScreenshotsPost**
> Screenshot apiV1AppsAppIdVersionsVersionIdScreenshotsPost(appId, versionId, body)

Create a screenshot for an appversion

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.ScreenshotsApi;


ScreenshotsApi apiInstance = new ScreenshotsApi();
String appId = "appId_example"; // String | the id of the app
String versionId = "versionId_example"; // String | the id of the version
ScreenshotPostRequest body = new ScreenshotPostRequest(); // ScreenshotPostRequest | the screenshot object to create
try {
    Screenshot result = apiInstance.apiV1AppsAppIdVersionsVersionIdScreenshotsPost(appId, versionId, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ScreenshotsApi#apiV1AppsAppIdVersionsVersionIdScreenshotsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **appId** | **String**| the id of the app |
 **versionId** | **String**| the id of the version |
 **body** | [**ScreenshotPostRequest**](ScreenshotPostRequest.md)| the screenshot object to create | [optional]

### Return type

[**Screenshot**](Screenshot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

