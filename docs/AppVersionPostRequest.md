# AppVersionPostRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** |  | 
**version** | **String** |  | 
**releaseNotes** | **String** |  |  [optional]
**blobUrl** | **String** |  | 
