# Screenshot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**versionId** | **String** |  | 
**url** | **String** |  | 
**description** | **String** |  |  [optional]
