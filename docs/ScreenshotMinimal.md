# ScreenshotMinimal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
