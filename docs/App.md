# App

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**title** | **String** |  | 
**description** | **String** |  |  [optional]
**_package** | **String** |  | 
**companyId** | **String** |  | 
