# DetailedApp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**_package** | **String** |  |  [optional]
**currentVersion** | [**AppVersion**](AppVersion.md) |  |  [optional]
**screenshots** | [**List&lt;ScreenshotMinimal&gt;**](ScreenshotMinimal.md) |  |  [optional]
