# AppVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**appId** | **String** |  | 
**version** | **String** |  | 
**releaseNotes** | **String** |  |  [optional]
**blobUrl** | **String** |  | 
