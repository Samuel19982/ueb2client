# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **String** |  | 
**city** | **String** |  | 
**state** | **String** |  |  [optional]
**postalCode** | **String** |  | 
**country** | **String** |  | 
