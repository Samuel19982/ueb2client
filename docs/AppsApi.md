# AppsApi

All URIs are relative to *https://appstore.education.cl2.smef.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiV1AppsGet**](AppsApi.md#apiV1AppsGet) | **GET** /api/v1/Apps | Returns a list of all apps
[**apiV1AppsIdDetailedGet**](AppsApi.md#apiV1AppsIdDetailedGet) | **GET** /api/v1/Apps/{id}/detailed | Returns the addressed app with the current version and screenshots
[**apiV1AppsIdPut**](AppsApi.md#apiV1AppsIdPut) | **PUT** /api/v1/Apps/id | Updates an app
[**apiV1AppsPost**](AppsApi.md#apiV1AppsPost) | **POST** /api/v1/Apps | Creates an app
[**deleteApp**](AppsApi.md#deleteApp) | **DELETE** /api/v1/Apps/{id} | Deletes an app
[**getApp**](AppsApi.md#getApp) | **GET** /api/v1/Apps/{id} | Returns the addressed app

<a name="apiV1AppsGet"></a>
# **apiV1AppsGet**
> List&lt;App&gt; apiV1AppsGet(count, offset)

Returns a list of all apps

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.AppsApi;


AppsApi apiInstance = new AppsApi();
Integer count = 56; // Integer | max number of apps to list
Integer offset = 56; // Integer | the offset for the requested data
try {
    List<App> result = apiInstance.apiV1AppsGet(count, offset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppsApi#apiV1AppsGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **count** | **Integer**| max number of apps to list | [optional]
 **offset** | **Integer**| the offset for the requested data | [optional]

### Return type

[**List&lt;App&gt;**](App.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsIdDetailedGet"></a>
# **apiV1AppsIdDetailedGet**
> DetailedApp apiV1AppsIdDetailedGet(id)

Returns the addressed app with the current version and screenshots

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.AppsApi;


AppsApi apiInstance = new AppsApi();
String id = "id_example"; // String | the id of the requested app
try {
    DetailedApp result = apiInstance.apiV1AppsIdDetailedGet(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppsApi#apiV1AppsIdDetailedGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the requested app |

### Return type

[**DetailedApp**](DetailedApp.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="apiV1AppsIdPut"></a>
# **apiV1AppsIdPut**
> apiV1AppsIdPut(body, id)

Updates an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.AppsApi;


AppsApi apiInstance = new AppsApi();
App body = new App(); // App | the object with changes
String id = "id_example"; // String | the id of the app to change
try {
    apiInstance.apiV1AppsIdPut(body, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppsApi#apiV1AppsIdPut");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**App**](App.md)| the object with changes | [optional]
 **id** | **String**| the id of the app to change | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

<a name="apiV1AppsPost"></a>
# **apiV1AppsPost**
> App apiV1AppsPost(body)

Creates an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.AppsApi;


AppsApi apiInstance = new AppsApi();
AppPostRequest body = new AppPostRequest(); // AppPostRequest | the app object to create
try {
    App result = apiInstance.apiV1AppsPost(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppsApi#apiV1AppsPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AppPostRequest**](AppPostRequest.md)| the app object to create | [optional]

### Return type

[**App**](App.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: application/json

<a name="deleteApp"></a>
# **deleteApp**
> deleteApp(id)

Deletes an app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.AppsApi;


AppsApi apiInstance = new AppsApi();
String id = "id_example"; // String | the id of the app
try {
    apiInstance.deleteApp(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppsApi#deleteApp");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the app |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getApp"></a>
# **getApp**
> App getApp(id)

Returns the addressed app

### Example
```java
// Import classes:
//import de.stl.saar.client.ApiException;
//import de.stl.saar.client.api.AppsApi;


AppsApi apiInstance = new AppsApi();
String id = "id_example"; // String | the id of the requested app
try {
    App result = apiInstance.getApp(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppsApi#getApp");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the requested app |

### Return type

[**App**](App.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

