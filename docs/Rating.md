# Rating

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**appId** | **String** |  | 
**author** | **String** |  | 
**text** | **String** |  | 
**timestamp** | **Integer** |  | 
**stars** | **Integer** | rating from 0 to 5 | 
